<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Clube;
use App\Socio;

class SocioController extends Controller
{
    
    private $clube;
    private $socio;

    public function __construct(Clube $clube, Socio $socio)
    {
        $this->middleware('auth');

        $this->socio = $socio;
        $this->clube = $clube;
    }

    
    public function index()
    {
        return view('socio.index', [
            'socios' => $this->socio->all()
        ]);
    }

    
    public function create()
    {
        return view('socio.create', [
            'clubes' => $this->clube->all()
        ]);
    }

    
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'clube_id' => 'max:255|integer',
        ]);

        $this->socio->create($request->all());

        return redirect()->route('socio.index');
    }
    
        
    public function edit(Socio $socio)
    {
        return view('socio.edit', [
            'clubes' => $this->clube->all(),
            'socio' => $socio
        ]);
    }

    
    public function update(Socio $socio, Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'clube_id' => 'max:255|integer',
        ]);


        $socio->name = $request->all()['name'];
        $socio->clube_id = ($request->all()['clube_id'] ?: "");
        $socio->save();

        return redirect()->route('socio.index');
    }
    
    public function destroy(Socio $socio)
    {

        $socio->delete();

        return redirect()->route('socio.index');
    }
}
