<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Clube;

class ClubeController extends Controller
{

    private $clube;
    
    public function __construct(Clube $clube)
    {
        $this->middleware('auth');
        $this->clube = $clube;
    }

    
    public function index()
    {

        return view('clube.index', [
            'clubes' => $this->clube->all()
        ]);
    }

    
    public function create()
    {
        return view('clube.create');
    }

    
    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|max:255',
        ]);

        $this->clube->create($request->all());

        return redirect()->route('clube.index');
    }


    public function edit(Clube $clube)
    {
        return view('clube.edit', [
            'clube' => $clube
        ]);
    }

    public function update(Clube $clube, Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
        ]);

        $clube->name = $request->all()['name'];
        $clube->save();

        return redirect()->route('clube.index');
    }
    
    public function destroy(Clube $clube)
    {

        $clube->delete();

        return redirect()->route('clube.index');
    }

}
