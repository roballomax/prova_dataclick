<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clube extends Model
{
    public $fillable = ['name'];
}
