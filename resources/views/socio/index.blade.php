@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Dashboard
                    <ul class="nav navbar-nav navbar-right" style='margin-right: 20px;'>
                        <li><a href='{{route("socio.create")}}'>Cadastrar Socios</a></li>
                    </ul>
                </div>

                <div class="panel-body">
                    <table>
                        <tr>
                            <th>Nome</th>
                            <th>Clube</th>
                            <th colspan='2'>Ações</th>
                        </tr>
                        @if(count($socios) > 0)
                            @foreach($socios as $socio)
                                <tr>
                                    <td>{{$socio['name']}} - </td>
                                    <td>{{$socio->clube->name}} - </td>
                                    <td><a href='{{route("socio.edit", $socio["id"])}}'>Editar</a> | <form style='float: right' action='{{route("socio.destroy", $socio["id"])}}' method='POST'>{{ csrf_field() }}{{ method_field('DELETE') }}<input type='submit' value='Deletar' /></form></td>
                                    
                                </tr>
                            @endforeach
                        @endif

                    </table>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
