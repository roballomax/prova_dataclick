@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Cadastro de Socio</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('socio.update', $socio->id) }}">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ (old('name') ?: $socio->name) }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        @if(count($clubes) > 0)
                            <div class="form-group{{ $errors->has('clube_id') ? ' has-error' : '' }}">
                                <label for="clube_id" class="col-md-4 control-label">Clube</label>

                                <div class="col-md-6">

                                    <select id="clube_id" type="text" class="form-control" name="clube_id" >
                                        <option value=''>Selecione um clube</option>
                                        @foreach($clubes as $clube)
                                            <option value='{{$clube["id"]}}' {{(old('clube_id') == $clube["id"] ? 'selected' : ($clube->id == $socio->clube->id ? 'selected' : ''))}}>{{$clube["name"]}}</option>
                                        @endforeach
                                    </select>

                                    @if ($errors->has('clube_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('clube_id') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        @endif

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Editar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
