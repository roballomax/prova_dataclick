@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Clubes
                    <ul class="nav navbar-nav navbar-right" style='margin-right: 20px;'>
                        <li><a href='{{route("clube.create")}}'>Cadastrar Clubes</a></li>
                    </ul>
                </div>

                <div class="panel-body">
                    <table>
                        <tr>
                            <th>Nome</th>
                            <th colspan='2'>Ações</th>
                        </tr>
                        @if(count($clubes) > 0)
                            @foreach($clubes as $clube)
                                <tr>
                                    <td>{{$clube['name']}}</td>
                                    <td><a href='{{route("clube.edit", $clube["id"])}}'>Editar</a> | <form style='float: right' action='{{route("clube.destroy", $clube["id"])}}' method='POST'>{{ csrf_field() }}{{ method_field('DELETE') }}<input type='submit' value='Deletar' /></form></td>
                                    
                                </tr>
                            @endforeach
                        @endif

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
